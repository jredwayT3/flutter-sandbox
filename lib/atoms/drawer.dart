import 'package:flutter/material.dart';
import 'package:sandbox/pages/camera.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sandbox/atoms/toast.dart';

getDrawer(context,widget, sharedPreferences){
  return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 135,
                      width: 135,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fitHeight,
                            image: new AssetImage('assets/logo.png'),
                          ),
                        ),
                      ),
                    )
                  ]),
              decoration: new BoxDecoration(color: Colors.lime),
            ),
            ListTile(
              leading: const Icon(Icons.list),
              title: Text('List Page'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/list");
              },
            ),
            ListTile(
              leading: const Icon(Icons.view_module),
              title: Text('Pokemon'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/products");
              },
            ),
            ListTile(
              leading: const Icon(Icons.all_inclusive),
              title: Text('Infinite Scroll'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/infinite_scroll");
              },
            ),
            ListTile(
              leading: const Icon(Icons.info),
              title: Text('About Page'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/about");
              },
            ),
            ListTile(
              leading: const Icon(Icons.tab),
              title: Text('Tabs'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/tabs");
              },
            ),
             ListTile(
              leading: const Icon(Icons.camera),
              title: Text('Camera'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CameraApp(widget.cameras)),
                );
              },
            ),
            ListTile(
              leading: const Icon(Icons.usb),
              title: Text('Debug Page'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/debug");
              },
            ),
            ListTile(
              leading: const Icon(Icons.transfer_within_a_station),
              title: Text('Log Out'),
              onTap: () {
                logout(sharedPreferences);
                Navigator.popAndPushNamed(context, "/login"); // Remove login page
              },
            ),
          ],
        ),
      );
      
}
void logout(sharedPreferences) {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sharedPreferences = sp;
      sharedPreferences.setString("user", "-1");
    });
    showToast("You have been logged out");
  }