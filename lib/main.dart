import 'package:flutter/material.dart';
import 'package:sandbox/pages/login.dart';
import 'package:sandbox/pages/home.dart';
import 'package:sandbox/util/routing.dart';
import 'package:sandbox/util/auth.dart';
import 'package:camera/camera.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<CameraDescription> cameras;
SharedPreferences sharedPreferences;

final ThemeData _themeData = new ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.lime,
    accentColor: Colors.limeAccent);

Future<void> main() async {
  cameras = await availableCameras();
  SharedPreferences.getInstance().then((SharedPreferences sp) {
    sharedPreferences = sp;
    var user = sharedPreferences.getString("user");
    runApp(MaterialApp(
      title: 'Flutter App',
      initialRoute: '/',
      routes: routes,
      theme: _themeData,
      debugShowCheckedModeBanner: false,
      home: (user != null && user != "-1")
          ? HomeScreen(cameras)
          : LoginPage(auth: new Auth(), cameras: cameras),
    ));
  });
}