import 'package:sandbox/pages/about.dart';
import 'package:sandbox/pages/list.dart';
import 'package:sandbox/pages/login.dart';
import 'package:sandbox/pages/debug.dart';
import 'package:sandbox/pages/tabs.dart';
import 'package:sandbox/pages/infinite_scroll.dart';
import 'package:sandbox/pages/products.dart';
import 'package:sandbox/pages/product_item.dart';
import 'package:sandbox/util/auth.dart';

final routes = {
  '/login' : (context) => new LoginPage(auth: new Auth()),
  '/debug': (context) => new InfoPage(),
  '/about': (context) => new AboutPage(),
  '/list' : (context) => new ListPage(),
  '/tabs' : (context) => new TabBarPage(),
  '/products' : (context) => new ProductPage(),
  '/infinite_scroll' : (context) => new InfiniteScrolling(),
  '/product_Item' : (context) => new ProductItemPage(pokemon: null),
};