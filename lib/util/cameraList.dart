import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
class InheritedCamera extends InheritedWidget{
  
  final List<CameraDescription> cameras;
  InheritedCamera({this.cameras, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
  
  static InheritedCamera of(BuildContext context) =>
    context.inheritFromWidgetOfExactType(InheritedCamera);

}