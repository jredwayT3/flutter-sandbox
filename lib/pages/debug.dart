import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoPage extends StatefulWidget {
  InfoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<InfoPage> {
  SharedPreferences sharedPreferences;
  String loginId;
  String token;
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sharedPreferences = sp;
      loginId = sharedPreferences.getString("user");
      token = sharedPreferences.getString("token");
      // will be null if never previously saved
      if (loginId == null) {
        loginId = "-1";
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("Debug Info"),
        ),
        body: new Container(
        padding: const EdgeInsets.all(16.0),
        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
        child: ListView(
          children: <Widget>
          [
            Center(child: Text("User ID:", style: TextStyle(fontWeight: FontWeight.bold))),
            Center(child: Text(this.loginId)),
            Center(child: Text("Firebase Token ID:", style: TextStyle(fontWeight: FontWeight.bold))),
            Center(child: Text(this.token)),
          ],
        ),
    ));
  }
}
