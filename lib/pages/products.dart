import 'package:flutter/material.dart';
import 'package:sandbox/pages/product_item.dart';
import 'package:sandbox/util/pokemon.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ProductPage extends StatefulWidget {
  @override
  ProductPageState createState() {
    return new ProductPageState();
  }
}

class ProductPageState extends State<ProductPage> {

  PokeHub pokeHub;

  @override
  void initState() {
    super.initState();

    fetchData();
  }

   fetchData() async {
    var res = await http.get("https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json");
    var decodedJson = jsonDecode(res.body);
    pokeHub = PokeHub.fromJson(decodedJson);
    print(pokeHub.toJson());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gen 1 Pokedex"),
      ),
      body: pokeHub == null ? Center(child: CircularProgressIndicator()) 
        : OrientationBuilder(builder: (context, orientation) {
        return Center(
          child: GridView.count(
            crossAxisCount: orientation == Orientation.portrait ? 2 : 4,
            children: pokeHub.pokemon.map((poke) =>
              Center(
                  child: new GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  ProductItemPage(pokemon: poke)),
                        );
                      },
                      child: new Card(
                          elevation: 1.5,
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height: 100.0,
                                width: 100.0,
                                child: Hero(
                                  tag: poke.img,
                                  child: DecoratedBox(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        fit: BoxFit.fill,
                                        image:
                                            new NetworkImage(poke.img),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Center(
                                  child: new Text(poke.name,
                                      style:
                                          Theme.of(context).textTheme.title)),
                            ],
                          )
                        )
                )
              )
            ).toList(),
          ),
        );
      }),
    );
  }
}
