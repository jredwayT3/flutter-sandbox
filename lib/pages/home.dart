import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sandbox/atoms/drawer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class HomeScreen extends StatefulWidget {
  final cameras;
  HomeScreen(this.cameras);

  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  SharedPreferences sharedPreferences;
  String _testValue;
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg){
        print("onLaunch called");
      },
      onResume: (Map<String, dynamic> msg){
        print("onResume called");
      },
      onMessage: (Map<String, dynamic> msg){
        print("onMessage called");
      }
    );
    
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
        sound: true,
        alert: true,
        badge: true
      )
    );
    firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings){
      print("IOS Settings Added");
    });
    
    firebaseMessaging.getToken().then((token) {
      print(token);
      SharedPreferences.getInstance().then((SharedPreferences sp) {
        sharedPreferences = sp;
        sharedPreferences.setString("token", token);
        setState(() {});
      });
    });

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sharedPreferences = sp;
      _testValue = sharedPreferences.getString("user");
      // will be null if never previously saved
      if (_testValue == null) {
        _testValue = "-1";
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return 
     Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Go To About'),
          onPressed: () {
            Navigator.pushNamed(context, "/about");
          },
        ),
      ),
      drawer: getDrawer(context, widget, sharedPreferences),
    );
  }
}
