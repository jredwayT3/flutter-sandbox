import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InfiniteScrolling extends StatefulWidget {
  InfiniteScrolling({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _InfiniteScrollingState createState() => new _InfiniteScrollingState();
}

class _InfiniteScrollingState extends State<InfiniteScrolling> {
  List<String> dogImages = new List();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    fetchFive();
    _scrollController.addListener((){
      if(_scrollController.position.pixels >= _scrollController.position.maxScrollExtent){
        fetchFive();
        print("Loading More");
      }
    });
  }

  @override 
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Infinite Scrolling"),
      ),
      body: dogImages.length == 0 ? Center(child: CircularProgressIndicator(),) : 
        ListView.builder(
        controller:  _scrollController,
        itemCount: dogImages.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              constraints: BoxConstraints.tightFor(height: 300),
              child: Image.network(dogImages[index], fit: BoxFit.fitWidth));
        },
      ),
    );
  }

  fetch() async {
    final response = await http.get('https://dog.ceo/api/breeds/image/random');
    if (response.statusCode == 200) {
      if(mounted) {
        setState(() {
          dogImages.add(json.decode(response.body)["message"]);
        });
      }
    } else {}
  }
  fetchFive(){
    for(int i = 0; i < 10; i++){
      fetch();
    }
  }
}
