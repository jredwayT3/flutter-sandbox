import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  @override
  AboutPageState createState() {
    return new AboutPageState();
  }
}

class AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("About Page"),
        ),
        body: Builder(builder: (BuildContext context) {
          return Center(
            child: ListView(
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    _showDialog();
                  },
                  child: Text('Show Alert'),
                ),
                RaisedButton(
                  onPressed: () {
                    _displaySnackBar(context);
                  },
                  child: Text('Show Bottom Alert'),
                )
              ],
            ),
          );
        }));
  }

  void _displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(content: Text('Are you talkin\' to me?'));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
